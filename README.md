# Apache web base container

A web container to use in development environments that require PHP running with Apache webserver.

Based off php:7.4-apache image.
